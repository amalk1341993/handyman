import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Tabcomponent } from './tabcomponent/tabcomponent';

@NgModule({
  declarations: [
    Tabcomponent,
  ],
  imports: [
  
  ],
  exports: [
    Tabcomponent
  ]
})
export class ComponentsModule {}
